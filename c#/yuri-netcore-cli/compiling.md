# Compiling with the *C# Compiler*

	csc -win32icon:"../../ico.ico" -out:yuri.exe Program.cs

csc path: `"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe"`

## References

* [C# Compiler Options](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/)
  * [C# Compiler Options Listed by Category](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/listed-by-category)
    * [-out](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/out-compiler-option)
    * [-win32icon](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/win32icon-compiler-option)
    * [-platform](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/platform-compiler-option)
  * [Command-line build with csc.exe](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/command-line-building-with-csc-exe)