# yuri.exe

A **CLI** (**C**ommand **L**ine **I**nterface) program that creates a text file ([`YURI.txt`](YURI.txt)) with 666 lines of the word YURI.

The program has been written in both [**Go**](golang/src/yuri.go) and [**C#**](c#/yuri-netcore-cli/Program.cs).