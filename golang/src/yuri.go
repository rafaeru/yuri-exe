package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
)

const yuritxt string = "YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI YURI"

func invalid() {
	fmt.Println("\nINVALID CODE\nRESTARTING PROGRAM…\n")
	time.Sleep(3 * time.Second)
	if runtime.GOOS == "windows" {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	} else {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	main()
}

func yuri() {
	f, err := os.Create("YURI.txt")
	if err != nil {
		fmt.Println(err)
		return
	}

	for i := 0; i < 665; i++ {
		f.WriteString(yuritxt + "\n")
		fmt.Println(yuritxt)
	}
	f.WriteString(yuritxt)
	time.Sleep(2 * time.Second)
	f.Close()
}

func main() {
	fmt.Println("INSERT CODE")
	var code string
	fmt.Scanln(&code)

	if strings.ToLower(code) == "yuri" {
		yuri()
	} else {
		invalid()
	}
}
